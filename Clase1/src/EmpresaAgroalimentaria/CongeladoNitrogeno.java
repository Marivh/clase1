/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EmpresaAgroalimentaria;

/**
 *
 * @author Mariana
 */
public class CongeladoNitrogeno extends ProductoCongelado {
    
    private String metodoCongelacion;
    private int exposicionNitrogenoSegundos;

    public CongeladoNitrogeno(String metodoCongelacion, int exposicionNitrogenoSegundos, String fechaCaducidad, String lote, String fecEnvasado, String paisOrigen) {
        super(fechaCaducidad, lote, fecEnvasado, paisOrigen);
        this.metodoCongelacion = metodoCongelacion;
        this.exposicionNitrogenoSegundos = exposicionNitrogenoSegundos;
    }
    
    public String getAtributos() {
        return "Fecha caducidad: " + fechaCaducidad
                + "\nLote: " + lote
                + "\nFecha de envasado: " + fecEnvasado
                + "\nPaís de origen: " + paisOrigen
                + "\nMétodo empleado para congelar: " + metodoCongelacion
                + "\nTiempo de exposición al nitrógeno: " + exposicionNitrogenoSegundos + " segundos";
    }

    public void tipoProducto() {
        System.out.println("Es un producto congelado por nitrógeno");
    }
    
}
