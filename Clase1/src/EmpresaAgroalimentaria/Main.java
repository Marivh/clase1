/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EmpresaAgroalimentaria;

/**
 *
 * @author Mariana
 */
public class Main {

    public static void main(String[] args) {

        Producto producto = new Producto("12/03/2020", "12344", "11/02/2019", "Panamá");
        ProductoFresco pf = new ProductoFresco("04/10/2019", "12145", "23/01/2019", "Costa Rica");
        ProductoRefrigerado pr = new ProductoRefrigerado("10/12/2024", "12345", "02/01/2019", "Alemania", 123, "-5° C");
        ProductoCongelado pc = new ProductoCongelado("24/05/2022", "12345", "03/02/2018", "Nicaragua");
        CongeladoAire cAire = new CongeladoAire(25, 25, 30, 20, "29/05/2024", "12345", "24/01/2018", "Estados Unidos");
        CongeladoAgua cAgua = new CongeladoAgua(25, "22/12/2025", "12345", "12/12/2018", "Estados Unidos");
        CongeladoNitrogeno cNitrogeno = new CongeladoNitrogeno("Nitrógeno", 40, "23/05/2020", "12345", "12/03/2017", "Canadá");

        System.out.println(producto.getAtributos());
        System.out.println("");
        System.out.println(pf.getAtributos());
        tipo(pf);
        System.out.println("");
        System.out.println(pr.getAtributos());
        tipo(pr);
        System.out.println("");
        System.out.println(pc.getAtributos());
        tipo(pc);
        System.out.println("");
        System.out.println(cAire.getAtributos());
        tipo(cAire);
        System.out.println("");
        System.out.println(cAgua.getAtributos());
        tipo(cAgua);
        System.out.println("");
        System.out.println(cNitrogeno.getAtributos());
        tipo(cNitrogeno);

    }

    public static void tipo(Producto producto) {
        producto.tipoProducto();
    }
}
