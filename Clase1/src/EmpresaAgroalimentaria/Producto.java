/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EmpresaAgroalimentaria;

/**
 *
 * @author Mariana
 */
public class Producto {

    public String fechaCaducidad;
    public String lote;
    public String fecEnvasado;
    public String paisOrigen;
    
    public Producto() {
    }

    public Producto(String fechaCaducidad, String lote, String fecEnvasado, String paisOrigen) {
        this.fechaCaducidad = fechaCaducidad;
        this.lote = lote;
        this.fecEnvasado = fecEnvasado;
        this.paisOrigen = paisOrigen;
    }


   public String getAtributos() {
        return "Fecha caducidad: " + fechaCaducidad
                + "\nLote: " + lote
                + "\nFecha de envasado: " + fecEnvasado
                + "\nPaís de origen: " + paisOrigen;
    }
    
    public void tipoProducto() {
        System.out.println("Qué tipo de producto es?");
    }
}
