/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EmpresaAgroalimentaria;

/**
 *
 * @author Mariana
 */
public class CongeladoAgua extends ProductoCongelado {
    
    private double salinidadAgua;

    public CongeladoAgua(double salinidadAgua, String fechaCaducidad, String lote, String fecEnvasado, String paisOrigen) {
        super(fechaCaducidad, lote, fecEnvasado, paisOrigen);
        this.salinidadAgua = salinidadAgua;
    }
    
    public String getAtributos() {
        return "Fecha caducidad: " + fechaCaducidad
                + "\nLote: " + lote
                + "\nFecha de envasado: " + fecEnvasado
                + "\nPaís de origen: " + paisOrigen
                + "\nSalinidad del agua: " + salinidadAgua;
    }

    public void tipoProducto() {
        System.out.println("Es un producto congelado por agua");
    }
    
}
