/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EmpresaAgroalimentaria;

/**
 *
 * @author Mariana
 */
public class ProductoRefrigerado extends Producto {

    private int codigoOSA;
    private String temperatura;

    public ProductoRefrigerado(String fechaCaducidad, String lote, String fecEnvasado, String paisOrigen, int codigoOSA, String temperatura) {
        super(fechaCaducidad, lote, fecEnvasado, paisOrigen);
        this.codigoOSA = codigoOSA;
        this.temperatura = temperatura;
    }

    public String getAtributos() {
        return "Fecha caducidad: " + fechaCaducidad
                + "\nLote: " + lote
                + "\nFecha de envasado: " + fecEnvasado
                + "\nPaís de origen: " + paisOrigen
                + "\nCodigo OSA: " + codigoOSA
                + "\nTemperatura: " + temperatura;
    }

    public void tipoProducto() {
        System.out.println("Es un producto refrigerado");
    }
}
