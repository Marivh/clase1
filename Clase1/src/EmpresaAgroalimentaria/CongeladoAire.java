/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EmpresaAgroalimentaria;

/**
 *
 * @author Mariana
 */
public class CongeladoAire extends ProductoCongelado {

    private int porcentajeNitrogeno;
    private int porcentajeOxigeno;
    private int porcentajeDioxidoCarbono;
    private int porcentajeVaporAgua;

    public CongeladoAire(int porcentajeNitrogeno, int porcentajeOxigeno, int porcentajeDioxidoCarbono, int porcentajeVaporAgua, String fechaCaducidad, String lote, String fecEnvasado, String paisOrigen) {
        super(fechaCaducidad, lote, fecEnvasado, paisOrigen);
        this.porcentajeNitrogeno = porcentajeNitrogeno;
        this.porcentajeOxigeno = porcentajeOxigeno;
        this.porcentajeDioxidoCarbono = porcentajeDioxidoCarbono;
        this.porcentajeVaporAgua = porcentajeVaporAgua;
    }

    public String getAtributos() {
        return "Fecha caducidad: " + fechaCaducidad
                + "\nLote: " + lote
                + "\nFecha de envasado: " + fecEnvasado
                + "\nPaís de origen: " + paisOrigen
                + "\nPorcentaje de Nitrogeno: " + porcentajeNitrogeno + "%"
                + "\nPorcentaje de Oxígeno: " + porcentajeOxigeno + "%"
                + "\nPorcentaje de Dióxido de Carbono: " + porcentajeDioxidoCarbono + "%"
                + "\nPorcentaje de vapor de agua: " + porcentajeVaporAgua + "%";
    }

    public void tipoProducto() {
        System.out.println("Es un producto congelado por aire");
    }

}
