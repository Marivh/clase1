/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencia;

/**
 *
 * @author Mariana
 */
public class Futbolista extends Personal {

    private int dorsal;
    private String demarcacion;

    public Futbolista(int id, String nombre, String apellidos, int edad, int dorsal, String demarcacion) {
        super(id, nombre, apellidos, edad);
        this.dorsal = dorsal;
        this.demarcacion = demarcacion;
    }

    public String getAtributos() {

        return "Id: " + id
                + "\nNombre: " + nombre
                + "\nApellidos:" + apellidos
                + "\nEdad: " + edad
                + "\nDorsal: " + dorsal
                + "\nDemarcación: " + demarcacion;
    }

    public void profesion() {
        System.out.println("El empleado es Futbolista");
    }
    
    public void jugarPartido(){
        
    }
    
    public void entrenar(){
        
    }
}
