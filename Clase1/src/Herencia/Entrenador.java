/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencia;

/**
 *
 * @author Mariana
 */
public class Entrenador extends Personal {

    private String idFederacion;

    public Entrenador(int id, String nombre, String apellidos, int edad, String idFederacion) {
        super(id, nombre, apellidos, edad);
        this.idFederacion = idFederacion;
    }

    public String getAtributos() {

        return "Id: " + id
                + "\nNombre: " + nombre
                + "\nApellidos:" + apellidos
                + "\nEdad: " + edad
                + "\nId Federación: " + idFederacion;
    }

    public void profesion() {
        System.out.println("El empleado es entrenador");
    }

    public void dirigirPartido(){
        
    }
    
    public void dirigirEntrenamiento(){
       
    }
}
