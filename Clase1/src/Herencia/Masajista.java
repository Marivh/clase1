/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencia;

/**
 *
 * @author Mariana
 */
public class Masajista extends Personal {

    private String titulacion;
    private int aniosExperiencia;

    public Masajista(int id, String nombre, String apellidos, int edad, String titulacion, int aniosExperiencia) {
        super(id, nombre, apellidos, edad);
        this.titulacion = titulacion;
        this.aniosExperiencia = aniosExperiencia;
    }

    public String getAtributos() {

        return "Id: " + id
                + "\nNombre: " + nombre
                + "\nApellidos:" + apellidos
                + "\nEdad: " + edad
                + "\nTitulación: " + titulacion
                + "\nAños de experiencia: " + aniosExperiencia;
    }

    public void profesion() {
        System.out.println("El empleado es masajista");
    }
    
    public void darMasaje(){
        
    }
}
