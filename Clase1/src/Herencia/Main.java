/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencia;

/**
 *
 * @author Mariana
 */
public class Main {

    public static void main(String[] args) {

        Personal persona = new Personal(207640644, "Mariana", "Vargas Huertas", 21);

        Futbolista futbolista = new Futbolista(109620764, "Carlos", "Rojas López", 24, 5, "Delantero");
        Entrenador entrenador = new Entrenador(208760567, "Felipe", "Murillo Zamora", 31, "6");
        Masajista masajista = new Masajista(208340456, "Lucia", "Carillo Mora", 30, "Diplomado", 10);

        System.out.println(persona.getAtributos());
        System.out.println("");
        System.out.println(futbolista.getAtributos());
        tipo(futbolista);
        System.out.println("");
        System.out.println(entrenador.getAtributos());
        tipo(entrenador);
        System.out.println("");
        System.out.println(masajista);
        tipo(masajista);

    }

    public static void tipo(Personal persona) {
        persona.profesion();
    }
}
