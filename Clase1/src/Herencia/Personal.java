/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencia;

/**
 *
 * @author Mariana
 */
public class Personal {
    
    public int id;
    public String nombre;
    public String apellidos;
    public int edad;

    public Personal() {
    }

    public Personal(int id, String nombre, String apellidos, int edad) {
        this.id = id;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.edad = edad;
    }

     public String getAtributos() {
        return "Id: " + id
                + "\nNombre: " + nombre
                + "\nApellidos:" + apellidos
                + "\nEdad: " + edad;
    }
    
    public void profesion() {
        System.out.println("Qué tipo de profesión es?");
    }
    
    public void concentrarse(){
        
    }
    
    public void viajar(){
        
    }
    
}
